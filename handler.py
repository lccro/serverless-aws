try:
  import unzip_requirements
except ImportError:
  pass

import boto3
import json
import os
from tempfile import mkstemp

import joblib
import pandas as pd
from sklearn.metrics import classification_report


def hello(event, context):
    s3 = boto3.client('s3')

    fd1, pkl = mkstemp(suffix="pkl")
    with os.fdopen(fd1, "wb") as f:
        s3.download_fileobj('smartfit.cyberlabs.ai', 'gato_model.pkl', f)

    fd2, csv = mkstemp(suffix="csv")
    with os.fdopen(fd2, "wb") as f:
        s3.download_fileobj('smartfit.cyberlabs.ai', 'last_week.csv', f)

    model = joblib.load(pkl)
    sets = pd.read_csv(csv)
    X_test, y_test = sets.drop([ 'dias_sem_ir','dias_sem_pagar','dist_casa','idade2', 'permanencia2', 'ind_acessos_total2', 'num_alunos', 'precipIndex', 'churn'], axis=1), sets['churn']
    # https://github.com/awslabs/serverless-application-model/issues/877
    pred = model.predict(X_test, thread_count=1)
    print(classification_report(y_test, pred))

    return { "statusCode": 200 }
