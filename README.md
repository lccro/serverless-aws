# Serverless @ AWS

## Pré-requisitos

  * framework [serverless](http://serverless.com/)
  * credenciais aws (key & secret)
  * instalar [aws-cli](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/cli-chap-install.html)
  * configurar aws-cli (configure)


## Criar um projeto

```sh
$ sls create -t aws-python3 -n nome-do-projeto
```


## Editar `serverless.yml`

  * definir `memorySize`
  * definir `iamRoleStatements` se necessário
  * definir as funções, env, eventos (se necessário)
  * se usar python packages, adicionar `package/exclude`
  * adicionar plugins (opcional)
    * `serverless-dotenv-plugin`
    * `serverless-python-requirements` ([documentação](https://www.npmjs.com/package/serverless-python-requirements))
      * dar uma olhada na documentação: `Dealing with Lambda's size limitations`

## ...
